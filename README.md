# README #

This repository contains the input files to the xppicorr ixpeobssim tool to correct the PI column.

The xppicorr.py tool in ixpeobssim corrects the energy of any observation.
It takes as input time dependent slope and offset available in this repository, 
and corrects the PI column of an observation provided by SOC

Example usage: 
xppicorr.py ixpe01001301_det1_evt2_v01.fits --corrfile casa_picorr_det1_v001.fits
Gives as output ixpe01001301_det1_evt2_v01_picorr.fits

On a longer timescale calibration based on cal sources will be included in SOC’s pipeline

## Old versions
To find an older version of a file, look at the git tags and checkout to the corresponding tag.
